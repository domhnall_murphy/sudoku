describe('Grid', function() {
  var grid = null;

  describe('initialization', function() {
    it('should take a DOM element representing the tiles and initialize this on the object', function() {
      grid = new SUDOKU.Grid("test");
      expect(grid.tiles).toEqual("test");
    });

    it('it should initialize working instance variable to null', function() {
      grid = new SUDOKU.Grid();
      expect(grid.working).toEqual(null);
    });
  });

  describe('instance methods', function() {
    beforeEach(function() {
      grid = new SUDOKU.Grid();
    });

    describe('#apply_solution_data', function() {
      it('should do something', function() {
      });
    });

    describe('#initialize_work_area', function() {
    });

    describe('#gather_initial_conitions', function() {
    });

    describe('#solve_next_cell', function() {
    });

    describe('#solve_problem', function() {
    });

    describe('#is_grid_solved', function() {
    });

    describe('#render_solution', function() {
    });

  });
});
