var types = ['String', 'Boolean', 'Array', 'Object'];

describe('GLOBAL', function() {
  describe('module method', function() {

    describe('#clazz', function() {
      it('should return a String', function() {
        expect( typeof GLOBAL.clazz({}) ).toEqual('string');
      });

      var test_instance_of_type = function(t) {
        var instance = new window[type]();

        it("should return the string '"+t+"' if passed and instance of type "+t, function() {
          expect(GLOBAL.clazz(instance)).toEqual(t);
        });
      };

      for(var i=0; i<types.length; i++) {
        var type = types[i];
        test_instance_of_type(type);
      }

    });

    describe('#is', function() { 
      it('should return a Boolean', function() {
        expect( typeof GLOBAL.is('Sting', {}) ).toEqual('boolean');
      });

      var test_instance_of_type = function(t) {
        var instance = new window[t]();

        var test_against_other_type = function(ot) { 
          it("should return false if testing for '"+ot+"'", function() {
            expect(GLOBAL.is(ot, instance)).toEqual(false);
          });
        };

        it("should return true if testing for '"+t+"'", function() {
          expect(GLOBAL.is(t, instance)).toEqual(true);
        });

        for(var j=0; j<types.length; j++) {
          var other_type = types[j];
          if ( other_type!==t ) { test_against_other_type(other_type); }
        };
      };

      for(var i=0; i<types.length; i++) {
        var type = types[i];
        describe("when passed an instance of type '"+type+"'", function() {
          test_instance_of_type(type);
        });
      }
    });
  });
});
