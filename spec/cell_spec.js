describe("Cell", function() {
  var cell;

  describe('construction', function() {
    cell = new SUDOKU.Cell(1,2,3,4);

    it('should set the row index appropriately', function() {
      expect(cell.row_index).toEqual(1);
    });

    it('should set the row index appropriately', function() {
      expect(cell.column_index).toEqual(2);
    });

    it('should set the sub grid index appropriately', function() {
      expect(cell.sub_grid_index).toEqual(3);
    });

    describe('where cell value is not defined', function() {
      beforeEach(function() {
        cell = new SUDOKU.Cell(1,2,3);
      });

      it('should initialize the options array to be of length 9', function() {
        expect(cell.options.length).toEqual(9);
      });

      it('should set all options to be true', function() {
        for(var i=0; i<cell.options.length; i++) {
          expect(cell.options[i]).toEqual(true);
        }
      });

      it('should set the user_defined attribute to false', function() {
        expect(cell.user_defined).toEqual(false);
      });
    });

    describe('where cell value is defined', function() {
      beforeEach(function() {
        cell = new SUDOKU.Cell(1,2,3,4);
      });

      it('should initialize the options array to be of length 9', function() {
        expect(cell.options.length).toEqual(9);
      });

      it('should set all options, except 4, to be false', function() {
        for(var i=0; i<cell.options.length; i++) {
          if(i===3) {
            expect(cell.options[i]).toEqual(true);
          } else {
            expect(cell.options[i]).toEqual(false);
          }
        }
      });

      it('should set the user_defined attribute to true', function() {
        expect(cell.user_defined).toEqual(true);
      });
    });
  });

  describe('instance methods', function() {
    beforeEach(function() {
      cell = new SUDOKU.Cell(0,0,0,9);
    });

    describe("#set_solution", function() {
      it("should throw an exception if the digit supplied <0", function() {
        expect(function() {
          cell.set_solution(-1);
        }).toThrow("Cell("+cell.row_index+","+cell.column_index+") must be assigned an integer value from 0-9");
      });

      it("should throw an exception if the digit supplied >9", function() {
        expect(function() {
          cell.set_solution(10);
        }).toThrow("Cell("+cell.row_index+","+cell.column_index+") must be assigned an integer value from 0-9");
      });
    });

    describe('#is_solved', function() {
      beforeEach(function() {
        cell = new SUDOKU.Cell(0,0,0);
      });

      it('should return true when there is only a single option left', function() {
        for(var i=1; i<cell.options.length; i++) {
          cell.options[i] = false;
        }
        expect(cell.is_solved()).toEqual(true);
      });

      for(var i=2; i<cell.options.length; i++) {
        it('should return false when there are ' + i + ' options left', function() {
          for(var j=i; j<cell.options.length; j++) { cell.options[j] = false }
          expect(cell.is_solved()).toEqual(false);
        });
      }
    });

    describe('#remove_option', function() {
      beforeEach(function() {
        cell = new SUDOKU.Cell(1,1,1);
      });

      it('should set the corresponding options element to false', function() {
        var option_to_remove = 5;
        var option_index = option_to_remove - 1;
        expect(cell.options[option_index]).toEqual(true);
        cell.remove_option(option_to_remove);
        expect(cell.options[option_index]).toEqual(false);
      });
    });

    describe('#solution', function() {
      it('should return null when the cell is not solved', function() {
        cell = new SUDOKU.Cell(1,2,3);
        expect(cell.solution()).toBeNull();
      });

      it('should return the value of the cell when it is solved', function() {
        cell = new SUDOKU.Cell(1,2,3,7);
        expect(cell.solution()).toEqual(7);
      });
    });
  });
});
