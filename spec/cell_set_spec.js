describe('CellSet', function() {
  describe('initialization', function() {
    var error_msg = "CellSet must be initialized with an array of 9 Cell objects.";
    var test_cells = [];
    for(var i=0; i<8; i++) { 
      test_cells[i] = new SUDOKU.Cell();
    }

    it('should throw an error if not passed an array', function() {
      expect(function() {
        new SUDOKU.CellSet("Blah");
      }).toThrow(error_msg);
    });

    it('should throw an error if not passed an array of 9 elements', function() {
      expect(function() {
        new SUDOKU.CellSet(test_cells);
      }).toThrow(error_msg);
    });

    it('should throw an error if not passed an array of 9 Cell objects', function() {
      expect(function() {
        var bad_cells = test_cells.concat(["blah"]);
        new SUDOKU.CellSet(bad_cells);
      }).toThrow(error_msg);
    });

    describe('when initialized with an array of 9 Cell objects', function() {
      var full_cells = test_cells.concat([new SUDOKU.Cell()]);
      var cell_set = null;
      beforeEach(function() { 
        cell_set = new SUDOKU.CellSet(full_cells);
      });

      it('should set the attribute cells', function() {
        expect(cell_set.cells).toEqual(full_cells);
      });
    });
  });
});
