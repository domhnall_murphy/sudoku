/* 
* Sudoku Solver
* Author: Domhnall Murphy
* Description: This file contains all code relating to the algorithm required to solve a given Sudoku problem.
*/

var SUDOKU_SOLVER = (function(ss) {

  ss.solve_user_defined_problem = function(full_solution_required) {
    ss.problem_grid.initialize_work_area();
    ss.problem_grid.gather_initial_conditions();
    full_solution_required ? ss.problem_grid.solve_problem() : ss.problem_grid.solve_next_cell();
    ss.problem_grid.render_solution();
  };

  ss.clear_grid = function() {
    ss.problem_grid.clear();
  };

  ss.populate_demo_data = function() {
    var demo_data = [ [7,5,9,6,2,1,4,8,3],
                      [6,3,1,8,7,4,9,2,5],
                      [8,4,2,9,5,3,7,6,1],
                      [1,7,5,4,6,2,3,9,8],
                      [4,2,8,3,9,5,6,1,7],
                      [9,6,3,7,1,8,5,4,2],
                      [3,9,6,2,8,7,1,5,4],
                      [2,1,4,5,3,6,8,7,9],
                      [5,8,7,1,4,9,2,3,6] ];

    ss.problem_grid.apply_solution_data(demo_data);
    ss.problem_grid.render_solution();
  };

  ss.set_problem = function() {
    var prob_data = [ [7,0,0,0,2,1,0,8,0],
                      [0,3,0,0,7,0,0,0,0],
                      [0,4,0,0,0,0,7,6,0],
                      [1,7,0,0,0,0,0,9,0],
                      [0,0,0,0,9,0,0,0,0],
                      [0,6,0,0,1,8,5,0,0],
                      [0,0,6,2,0,0,0,0,4],
                      [0,0,4,5,0,0,8,0,9],
                      [5,0,0,0,0,0,0,0,0] ];

    ss.problem_grid.apply_solution_data(prob_data);
    ss.problem_grid.render_solution();
  };

  return ss;

}(SUDOKU_SOLVER || {}));
