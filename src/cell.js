var SUDOKU = (function(self) {
  self.Cell = function(row_index, column_index, sub_grid_index, value) {
    this.row_index = row_index;
    this.column_index = column_index;
    this.sub_grid_index = sub_grid_index;
    this.user_defined = false;
    this.options = [];
    for(var i=0; i<9; i++) this.options[i] = true;

    if ((typeof value)!=='undefined' && value!==0) {
      this.user_defined = true;
      this.set_solution(value);
    }
  };

  self.Cell.prototype.is_solved = function() {
    var total_possibilities = this.options.reduce(function(x,y){ if (y) x++; return x; }, 0);
    return total_possibilities===1
  };

  self.Cell.prototype.solution = function() {
    if(!this.is_solved()) return null;
    for(var i=0; i<9; i++) { 
      if(this.options[i]) {
        return i+1; 
      }
    }
  };

  self.Cell.prototype.remove_option = function(digit) {
    this.options[digit-1] = false;
  };

  self.Cell.prototype.set_solution = function(digit) {
    if (digit<0 || digit>9) throw new Error("Cell("+this.row_index+","+this.column_index+") must be assigned an integer value from 0-9");
    for(var i=0; i<9; i++) {
      this.options[i] = false;
      if(digit==(i+1)) this.options[i] = true;
    }
  };

  return self;
})(SUDOKU || {});
