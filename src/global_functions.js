var GLOBAL = (function(self){
  // is(type, obj)
  // Method is intended to check if the type/class of a given object 'obj' is 'type'.
  // type is a String
  // obj is any object instance
  self.is = function(type, obj) { 
    return obj !== undefined && obj !== null && GLOBAL.clazz(obj)===type;
  };

  // clazz(obj)
  // Method returning a string representation of the class/type of the object 'obj'
  self.clazz = function(obj) {
    return Object.prototype.toString.call(obj).slice(8,-1);
  };

  return self;
})(GLOBAL || {});
