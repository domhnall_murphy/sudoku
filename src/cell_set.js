var SUDOKU = (function(self) {
  var initialization_error_msg = "CellSet must be initialized with an array of 9 Cell objects.";

  self.CellSet = function(cells) {
    if(!GLOBAL.is('Array', cells)) throw new Error(initialization_error_msg);
    if (cells.length!==9) throw new Error(initialization_error_msg);
    var all_cells = cells.reduce(function(x,y){ return (x && (y instanceof SUDOKU.Cell)) }, true);
    if (!all_cells) throw new Error(initialization_error_msg);
    this.cells = cells;
  };

  self.CellSet.prototype.accept = function(visitor) {
    visitor.apply(self);
  };

  return self;

})(SUDOKU || {});
