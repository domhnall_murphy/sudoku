var SUDOKU = (function(self) {

  self.Grid = function (tiles) {
    this.tiles = tiles;
    this.working = null;
    this.initialize_work_area();
  };

  self.Grid.prototype.apply_solution_data = function(solution_data) {
    this.initialize_work_area();
    for(var i=1; i<10; i++) {
      for(var j=1; j<10; j++) {
        if(solution_data[i-1][j-1]!=0) this.working[i][j].set_solution(solution_data[i-1][j-1]);
      }
    }
  };

  self.Grid.prototype.initialize_work_area = function() {
    this.working = [];
    for(var i=1; i<10; i++) {
      this.working[i] = [];
      for(var j=1; j<10; j++) {
        this.working[i][j] = new SUDOKU.Cell(0);
      }
    }
  };

  self.Grid.prototype.gather_initial_conditions = function() {
    var that = this;
    $.each(this.tiles, function(index, el) {
      var element_content = $(el).html();
      var cell = that.working[el.dataset.rowIndex][el.dataset.columnIndex];
      if(element_content>0 && element_content<10) {
        cell.user_defined = true;
        cell.set_solution(element_content);
      }
    });
  };

  self.Grid.prototype.solve_problem = function() {
    while(!this.is_grid_solved()) {
      this.solve_next_cell();
    }
  };

  self.Grid.prototype.solve_next_cell = function() {
    var counter = 0;

    var update_for_first_order = function(row_index, column_index) {
      if(working[row_index][column_index].is_solved()) return;
      //apply_first_order_correlations(row_cells);
    };

    while(counter<20) {
      for(var i=1; i<10; i++) {
        for(var j=1; j<10; j++) {
          if (!this.working[i][j].is_solved()) {
            update_for_first_order(i, j);
            if(this.working[i][j].is_solved()) return;
            update_for_second_order(i, j);
            if(this.working[i][j].is_solved()) return;
          }
        }
      }
      counter++;
    }
    throw new Error("Unable to solve for next cell value.  Please check that the problem details are correct?");
  };


  self.Grid.prototype.is_grid_solved = function() {
    is_solved = true
    for(var i=1; i<10; i++) {
      for(var j=1; j<10; j++) {
        is_solved = is_solved && this.working[i][j].is_solved();
        if (!is_solved) return false;
      }
    }
    return is_solved
  };

  return self;
})(SUDOKU || {});
