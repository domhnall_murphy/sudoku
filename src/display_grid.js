var SUDOKU = (function(self) {

  self.DisplayGrid = function(tiles) {
    if (!instanceof SUDOKU.Grid) throw new Error("DisplayGrid must be initialized with a Grid object");
    this.tiles = tiles;
    this.grid = new SUDOKU.Grid(tiles);
  };

  self.DisplayGrid.prototype.clear = function() {
    $.each(this.tiles, function(index, el) {
      $(el).html('&nbsp;');
      $(el).removeClass('user_defined');
    });
  };

  self.DisplayGrid.prototype.render_solution = function() {
    var that = this;
    $.each(this.tiles, function(index, el) {
      var element = $(el);
      var cell = grid.working[el.dataset.rowIndex][el.dataset.columnIndex];
      if (cell.user_defined) element.addClass('user_defined');
      element.html(cell.is_solved() ? cell.solution() : '&nbsp;');
    });
  };

  return self;
})(SUDOKU || {});
