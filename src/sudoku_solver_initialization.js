/* 
* Sudoku Solver
* Author: Domhnall Murphy
* Description: This file contains all code relating to the initialization of the Sudoku Solver UI.
*/

var SUDOKU_SOLVER = (function($, ss) {
  ss.puzzle_area_selector = '#puzzle_area';
  ss.number_tiles_selector = '#number_tiles_area';
  ss.timings_area_selector = '#timings_area';
  ss.puzzle_grid_id = 'puzzle_grid';
  ss.puzzle_tile_class = 'puzzle_tile';
  ss.problem_grid = null;

  ss.init = function(){
    ss.sub_grid_counter = counter();
    init_puzzle_area();
    init_number_tiles_area();
    var tiles = $('.'+ss.puzzle_tile_class);
    ss.problem_grid = new SUDOKU.Grid(tiles);
  };

  var counter = function() {
    var n = 1;
    return {
      count: function() { return n++; },
      reset: function() { n = 0; }
    };
  };

  var init_puzzle_area = function(){
    var container = $(SUDOKU_SOLVER.puzzle_area_selector);
    build_puzzle_grid(container);
    append_button(container, 'solve_problem', 'Solve', solve_for_full_grid);
    append_button(container, 'hint', 'Give me a hint', solve_for_next_cell);
    append_button(container, 'clear_grid', 'Clear', ss.clear_grid);
    append_button(container, 'set_problem', 'Set problem', ss.set_problem);
    append_button(container, 'load_demo', 'Load demo', ss.populate_demo_data);
  };

  var solve_for_full_grid = function() {
    timed_invocation(ss, 'solve_user_defined_problem', true);
  };

  var solve_for_next_cell = function() {
    timed_invocation(ss, 'solve_user_defined_problem', false);
  };

  var timed_invocation = function(target, method) {
    var args = Array.prototype.slice.call(arguments, 2);
    var start_time = Date.now();
    ss.solve_user_defined_problem(true);
    try {
      ss[method].apply(args);
    } catch(ex) {
      alert("Unable to solve this problem" + ex);
    }
    var end_time = Date.now();
    output_timings(method, end_time-start_time);
  };

  var output_timings = function(method, time_in_ms) {
    var timings_section = $(SUDOKU_SOLVER.timings_area_selector);
    var timings_element = "<p class='timing'>Method '" + method + "' was invoked and took " + time_in_ms + "ms to complete.</p>";
    if(timings_section.length>0) {
      timings_section.removeClass('hidden');
      timings_section.append(timings_element);
    }
  };

  var init_number_tiles_area = function(){
    var container = $(SUDOKU_SOLVER.number_tiles_selector);
    for(var i=1; i<10; i++) {
      build_number_tile(container, i);
    }
    build_number_tile(container, '&nbsp;');
  };

  var build_puzzle_grid = function(container) {
    var puzzle_grid = "<table id='puzzle_grid'></table>"
    container.append(puzzle_grid);
    puzzle_grid = container.find('table').last();
    for(var i=1; i<4; i++) {
      build_puzzle_row(puzzle_grid, i);
    }
  };

  var append_button = function(container, id, text, callback) {
    var button = "<button id='" + id + "'>" + text + "</button>";
    container.after(button);
    button = $('button#' + id).first();
    button.click(callback);
  };

  var build_puzzle_row = function(container, row_index) {
    var row = "<tr></tr>";
    container.append(row);
    row = container.find('tr').last();
    for(var j=1; j<4; j++) {
      build_sub_grid(row, row_index, j);
    }
  };

  var build_sub_grid = function(container, outer_row_index, outer_column_index) {
    var sub_grid_index = SUDOKU_SOLVER.sub_grid_counter.count();

    var build_sub_grid_row = function(container, inner_row_index) {
      var sub_grid_row = "<tr class='sub_grid_row'></tr>";
      container.append(sub_grid_row);
      sub_grid_row = container.find('tr.sub_grid_row').last();
      for(var j=1; j<4; j++) {
        var row_index = (outer_row_index-1)*3 + inner_row_index;
        var column_index = (outer_column_index-1)*3 + j;
        build_puzzle_tile(sub_grid_row, row_index, column_index, sub_grid_index);
      }
    };

    var build_puzzle_tile = function(container, row_index, column_index, sub_grid_index) {
      var puzzle_tile = "<td class='tile puzzle_tile' data-row-index='" + row_index + "' data-column-index='" + column_index + "' data-sub-grid-index='" + sub_grid_index + "'>&nbsp;</td>";
      container.append(puzzle_tile);
      puzzle_tile = container.find('.tile.puzzle_tile').last();
      puzzle_tile.droppable({
        hoverClass: "hover_over",
        drop: function( event, ui ) {
          var number = $(ui.helper)[0].dataset.number;
          var row_index = this.dataset.rowIndex;
          var column_index = this.dataset.columnIndex;
          var sub_grid_index = this.dataset.subGridIndex;
          $(this).addClass( "ui-state-highlight" ).html( number );
        }
      });
    };

    var sub_grid = "<td><table class='sub_grid'></table></td>";
    container.append(sub_grid);
    sub_grid = container.find('table.sub_grid').last();
    for(var i=1; i<4; i++) {
      build_sub_grid_row(sub_grid, i);
    }
  };

  var build_number_tile = function(container, number) {
    var number_tile = "<div class='tile number_tile' data-number='" + number + "'>" + number + "</div>";
    container.append(number_tile);
    number_tile = container.find('.tile.number_tile').last();
    number_tile.draggable({
      opacity: 0.7,
      helper: "clone",
      start: function(event, ui) {
        var test_element = $(".tile.number_tile")[0];
        var width = test_element.getBoundingClientRect().width;
        var height = test_element.getBoundingClientRect().height;
        $(ui.helper)[0].style.width = width + 'px';
        $(ui.helper)[0].style.height = height + 'px';
      }
    });
  };

  return ss;

}(jQuery, SUDOKU_SOLVER || {}));

$(document).ready(function() {
  SUDOKU_SOLVER.init();
});
